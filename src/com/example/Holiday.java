package com.example;

import java.util.Objects;

public class Holiday {
    private String name;
    private int day;
    private String month;

    public Holiday(String name  ,int day ,String month) {
        this.name = name;
        this.day =day;
        this.month = month;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Holiday holiday = (Holiday) o;
        return day == holiday.day && Objects.equals(name, holiday.name) && Objects.equals(month, holiday.month);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, day, month);
    }

    public  boolean inSameMonth(Holiday holiday){
        return this.month.equals(holiday.month);
    }
   public static double avgDate(Holiday[] holiday){
        int sum = 0;
        for (int i=0; i< holiday.length; i++){
            sum += holiday[i].day;
        }
        double average = sum/holiday.length;
        return average;
   }

    @Override
    public String toString() {
        return "Holiday{" +
                "name='" + name + '\'' +
                ", day=" + day +
                ", month='" + month + '\'' +
                '}';
    }
}
